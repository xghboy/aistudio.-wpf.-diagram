﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Linq;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.Mind.Models;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Mind.Helpers
{
    public static class MindThemeHelper
    {
        static MindThemeHelper()
        {
            MindThemeDictionary = new List<MindThemeModel>()
            {
                SkyBlueTheme,
                SkyBlueMiniTheme,
                LiteratureGreenTheme,
                LiteratureGreenMiniTheme,
                BrainDeadPinkTheme,
                BrainDeadPinkMiniTheme,
                RomanticPurpleTheme,
                RomanticPurpleMiniTheme,
                FreshRedTheme,
                FreshRedMiniTheme,
                EarthyYellowTheme,
                EarthyYellowMiniTheme,
                CoolLightYellowTheme,
                CoolLightYellowMiniTheme
            };
        }

        public static MindThemeModel GetTheme(MindTheme theme)
        {
            return MindThemeDictionary.FirstOrDefault(p => p.Theme == theme) ?? SkyBlueTheme;
        }

        public static readonly List<MindThemeModel> MindThemeDictionary;

        public static MindThemeModel SkyBlueTheme = new MindThemeModel()
        {
            Theme = MindTheme.SkyBlue,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                LineColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(50, 15),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xee, 0xf3, 0xf6),
                LineColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            }
        };
        
        public static MindThemeModel SkyBlueMiniTheme= new MindThemeModel()
        {
            Theme = MindTheme.SkyBlueMini,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                LineColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xee, 0xf3, 0xf6),
                LineColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0x73, 0xa1, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            }
        };
        
        public static MindThemeModel LiteratureGreenTheme = new MindThemeModel()
        {
            Theme = MindTheme.LiteratureGreen,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0x73, 0xbf, 0x76),
                LineColor = Color.FromRgb(0x73, 0xbf, 0x76),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(50, 15),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xee, 0xf6, 0xee),
                LineColor = Color.FromRgb(0x73, 0xbf, 0x76),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0x73, 0xbf, 0x76),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            }
        };
        
        public static MindThemeModel LiteratureGreenMiniTheme = new MindThemeModel()
        {
            Theme = MindTheme.LiteratureGreenMini,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0x73, 0xbf, 0x76),
                LineColor = Color.FromRgb(0x73, 0xbf, 0x76),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xee, 0xf6, 0xee),
                LineColor = Color.FromRgb(0x73, 0xbf, 0x76),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0x73, 0xbf, 0x76),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            }
        };
        
        public static MindThemeModel BrainDeadPinkTheme = new MindThemeModel()
        {
            Theme = MindTheme.BrainDeadPink,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xbf, 0x73, 0x94),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x94),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(50, 15),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xf6, 0xee, 0xf2),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x94),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0xbf, 0x73, 0x94),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            }
        };
        
        public static MindThemeModel BrainDeadPinkMiniTheme =  new MindThemeModel()
        {
            Theme = MindTheme.BrainDeadPinkMini,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xbf, 0x73, 0x94),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x94),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xf6, 0xee, 0xf2),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x94),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0xbf, 0x73, 0x94),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            }
        };
        
        public static MindThemeModel RomanticPurpleTheme = new MindThemeModel()
        {
            Theme = MindTheme.RomanticPurple,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                LineColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(50, 15),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xef, 0xee, 0xf6),
                LineColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            }
        };
        
        public static MindThemeModel RomanticPurpleMiniTheme = new MindThemeModel()
        {
            Theme = MindTheme.RomanticPurpleMini,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                LineColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xef, 0xee, 0xf6),
                LineColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0x7b, 0x73, 0xbf),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            }
        };
        
        public static MindThemeModel FreshRedTheme = new MindThemeModel()
        {
            Theme = MindTheme.FreshRed,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xbf, 0x73, 0x73),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x73),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(50, 15),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xf6, 0xee, 0xee),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0xbf, 0x73, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            }
        };
        
        public static MindThemeModel FreshRedMiniTheme =  new MindThemeModel()
        {
            Theme = MindTheme.FreshRedMini,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xbf, 0x73, 0x73),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x73),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xf6, 0xee, 0xee),
                LineColor = Color.FromRgb(0xbf, 0x73, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0xbf, 0x73, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            }
        };
        
        public static MindThemeModel EarthyYellowTheme = new MindThemeModel()
        {
            Theme = MindTheme.EarthyYellow,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xbf, 0x93, 0x73),
                LineColor = Color.FromRgb(0xbf, 0x93, 0x73),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(50, 15),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xf6, 0xf2, 0xee),
                LineColor = Color.FromRgb(0xbf, 0x93, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0xbf, 0x93, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            }
        };
        
        public static MindThemeModel EarthyYellowMiniTheme= new MindThemeModel()
        {
            Theme = MindTheme.EarthyYellowMini,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xbf, 0x93, 0x73),
                LineColor = Color.FromRgb(0xbf, 0x93, 0x73),
                FontColor = Colors.White,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xf6, 0xf2, 0xee),
                LineColor = Color.FromRgb(0xbf, 0x93, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.Transparent,
                LineColor = Color.FromRgb(0xbf, 0x93, 0x73),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            }
        };
        
        public static MindThemeModel CoolLightYellowTheme = new MindThemeModel()
        {
            Theme = MindTheme.CoolLightYellow,
            Dark = true,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xe9, 0xdf, 0x98),
                LineColor = Color.FromRgb(0xe9, 0xdf, 0x98),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(50, 15),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xa4, 0xc5, 0xc0),
                LineColor = Color.FromRgb(0xa4, 0xc5, 0xc0),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.White,
                LineColor = Colors.White,
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(15, 15),
            }
        };
        
        public static MindThemeModel CoolLightYellowMiniTheme = new MindThemeModel()
        {
            Theme = MindTheme.CoolLightYellowMini,
            Dark = true,
            MindThemeLevel0 = new MindThemeModelItem()
            {
                ItemWidth = 120,
                ItemHeight = 40,
                FillColor = Color.FromRgb(0xe9, 0xdf, 0x98),
                LineColor = Color.FromRgb(0xe9, 0xdf, 0x98),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel1 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Color.FromRgb(0xa4, 0xc5, 0xc0),
                LineColor = Color.FromRgb(0xa4, 0xc5, 0xc0),
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            },
            MindThemeLevel2 = new MindThemeModelItem()
            {
                ItemWidth = 100,
                ItemHeight = 25,
                FillColor = Colors.White,
                LineColor = Colors.White,
                FontColor = Colors.Black,
                FontSize = 15,
                Spacing = new SizeBase(10, 10),
            }
        };        

        public static void ThemeChange(MindNode mindNode, MindTheme mindTheme, bool initAppearance = false)
        {
            var mindThemeModel = GetTheme(mindTheme);
            switch (mindNode.NodeLevel)
            {
                case 0:
                    {
                        if (initAppearance)
                        {
                            mindNode.ItemWidth = mindThemeModel.MindThemeLevel0.ItemWidth;
                            mindNode.ItemHeight = mindThemeModel.MindThemeLevel0.ItemHeight;
                        }
                        mindNode.ColorViewModel.FillColor.Color = mindThemeModel.MindThemeLevel0.FillColor;
                        mindNode.ColorViewModel.LineColor.Color = mindThemeModel.MindThemeLevel0.LineColor;
                        mindNode.FontViewModel.FontColor = mindThemeModel.MindThemeLevel0.FontColor;
                        mindNode.FontViewModel.FontSize = mindThemeModel.MindThemeLevel0.FontSize;
                        mindNode.Spacing = mindThemeModel.MindThemeLevel0.Spacing;
                        break;
                    }
                case 1:
                    {
                        if (initAppearance)
                        {
                            mindNode.ItemWidth = mindThemeModel.MindThemeLevel1.ItemWidth;
                            mindNode.ItemHeight = mindThemeModel.MindThemeLevel1.ItemHeight;
                        }
                        mindNode.ColorViewModel.FillColor.Color = mindThemeModel.MindThemeLevel1.FillColor;
                        mindNode.ColorViewModel.LineColor.Color = mindThemeModel.MindThemeLevel1.LineColor;
                        mindNode.FontViewModel.FontColor = mindThemeModel.MindThemeLevel1.FontColor;
                        mindNode.FontViewModel.FontSize = mindThemeModel.MindThemeLevel1.FontSize;
                        mindNode.Spacing = mindThemeModel.MindThemeLevel1.Spacing;
                        break;
                    }
                default:
                    {
                        if (initAppearance)
                        {
                            mindNode.ItemWidth = mindThemeModel.MindThemeLevel2.ItemWidth;
                            mindNode.ItemHeight = mindThemeModel.MindThemeLevel2.ItemHeight;
                        }
                        mindNode.ColorViewModel.FillColor.Color = mindThemeModel.MindThemeLevel2.FillColor;
                        mindNode.ColorViewModel.LineColor.Color = mindThemeModel.MindThemeLevel2.LineColor;
                        mindNode.FontViewModel.FontColor = mindThemeModel.MindThemeLevel2.FontColor;
                        mindNode.FontViewModel.FontSize = mindThemeModel.MindThemeLevel2.FontSize;
                        mindNode.Spacing = mindThemeModel.MindThemeLevel2.Spacing;
                        break;
                    }
            }
        }

        public static MindThemeModel GetNodeTheme(MindNode mindNode)
        {
            MindThemeModel mindThemeModel = new MindThemeModel()
            {
                MindThemeLevel0 = new MindThemeModelItem()
                {
                    ItemWidth = mindNode.ItemWidth,
                    ItemHeight = mindNode.ItemHeight,
                    FillColor = mindNode.ColorViewModel.FillColor.Color,
                    LineColor = mindNode.ColorViewModel.LineColor.Color,
                    FontColor = mindNode.FontViewModel.FontColor,
                    FontSize = mindNode.FontViewModel.FontSize,
                    Spacing = mindNode.Spacing,
                },
            };
            return mindThemeModel;
        }

        public static void SetThemeModel(MindNode mindNode, MindThemeModel mindThemeModel)
        {
            mindNode.ItemWidth = mindThemeModel.MindThemeLevel0.ItemWidth;
            mindNode.ItemHeight = mindThemeModel.MindThemeLevel0.ItemHeight;
            mindNode.ColorViewModel.FillColor.Color = mindThemeModel.MindThemeLevel0.FillColor;
            mindNode.ColorViewModel.LineColor.Color = mindThemeModel.MindThemeLevel0.LineColor;
            mindNode.FontViewModel.FontColor = mindThemeModel.MindThemeLevel0.FontColor;
            mindNode.FontViewModel.FontSize = mindThemeModel.MindThemeLevel0.FontSize;
            mindNode.Spacing = mindThemeModel.MindThemeLevel0.Spacing;
        }

        public static MindThemeModelItem GetNodeDefaultTheme(MindNode mindNode)
        {
            var defaultMindTheme = MindThemeDictionary.FirstOrDefault(p => p.Theme == mindNode.MindTheme) ?? SkyBlueTheme;
            switch(mindNode.NodeLevel)
            {
                case 0: return defaultMindTheme.MindThemeLevel0;
                case 1: return defaultMindTheme.MindThemeLevel1;
                default: return defaultMindTheme.MindThemeLevel2;
            }
        }

        public static IColorViewModel GetColorViewModel(MindNode mindNode)
        {
            ColorViewModel colorViewModel = new ColorViewModel();
            CopyHelper.CopyPropertyValue(mindNode.ColorViewModel, colorViewModel);
            return colorViewModel;
        }

        public static IFontViewModel GetFontViewModel(MindNode mindNode)
        {
            FontViewModel fontViewModel = new FontViewModel();
            CopyHelper.CopyPropertyValue(mindNode.FontViewModel, fontViewModel);
            return fontViewModel;
        }
    }

    public class MindThemeModelItem
    {
        public double ItemWidth
        {
            get; set;
        }
        public double ItemHeight
        {
            get; set;
        }
        public Color FillColor
        {
            get; set;
        }
        public Color LineColor
        {
            get; set;
        }
        public Color FontColor
        {
            get; set;
        }
        public double FontSize
        {
            get; set;
        }
        public SizeBase Spacing
        {
            get; set;
        }
    }

    public class MindThemeModel
    {
        public MindTheme Theme
        {
            get; set;
        }
        public string Name
        {
            get
            {
                return Theme.GetDescription();
            }
        }
        public bool Dark
        {
            get; set;
        }
        public MindThemeModelItem MindThemeLevel0
        {
            get; set;
        }
        public MindThemeModelItem MindThemeLevel1
        {
            get; set;
        }
        public MindThemeModelItem MindThemeLevel2
        {
            get; set;
        }
    }
}
