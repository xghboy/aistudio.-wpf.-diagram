﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using System.Xml.Linq;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.Mind.ViewModels;
using AIStudio.Wpf.DiagramDesigner.Serializable;

namespace AIStudio.Wpf.Mind.Controls
{
    /// <summary>
    /// MindEditor.xaml 的交互逻辑
    /// </summary>
    [TemplatePart(Name = PART_DiagramControl, Type = typeof(DiagramControl))]
    [TemplatePart(Name = PART_ContentControl, Type = typeof(ContentControl))]
    [TemplatePart(Name = PART_SearchControl, Type = typeof(Border))]
    public partial class MindEditor : UserControl
    {
        public const string PART_DiagramControl = "PART_DiagramControl";
        public const string PART_ContentControl = "PART_ContentControl";
        public const string PART_SearchControl = "PART_SearchControl";
        private DiagramControl _diagramControl;
        private ContentControl _contentControl;
        private Border _searchborder;

        private MindDiagramViewModel _diagramViewModel;

        static MindEditor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MindEditor), new FrameworkPropertyMetadata(typeof(MindEditor)));
        }

        public MindEditor()
        {
            _diagramViewModel = new MindDiagramViewModel();
            _diagramViewModel.DiagramOption.LayoutOption.GridMarginSize = new Size(0, 0);
            _diagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            _diagramViewModel.DiagramOption.LayoutOption.PageSize = new SizeBase(1000d, 1000d);
            _diagramViewModel.DiagramOption.LayoutOption.ShowGrid = false;
            _diagramViewModel.DefaultZoomBox = true;

            _diagramViewModel.PropertyChanged += DiagramViewModel_PropertyChanged;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _diagramControl = GetTemplateChild(PART_DiagramControl) as DiagramControl;
            _diagramControl.HorizontalAlignment = HorizontalAlignment.Stretch;
            _diagramControl.VerticalAlignment = VerticalAlignment.Stretch;   
            _diagramControl.DataContext = _diagramViewModel;

            _contentControl = GetTemplateChild(PART_ContentControl) as ContentControl;
            if (_contentControl != null)
            {
                _contentControl.DataContext = _diagramViewModel;
            }

            _searchborder = GetTemplateChild(PART_SearchControl) as Border;
            if (_searchborder != null)
            {
                _searchborder.DataContext = _diagramViewModel;
            }

            GetDataFunc = GetData;
        }

        private void DiagramViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                SelectedObject = _diagramViewModel.SelectedItem;
            }
        }

        //一点要绑定不为空的FlowchartModel才能用，即便为空的也要new一个再来绑定
        public static readonly DependencyProperty DataProperty =
          DependencyProperty.Register(nameof(Data),
                                     typeof(string),
                                     typeof(MindEditor),
                                     new FrameworkPropertyMetadata(null, OnDataChanged));

        public string Data
        {
            get
            {
                return (string)GetValue(DataProperty);
            }
            set
            {
                SetValue(DataProperty, value);
            }
        }
        private static void OnDataChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as MindEditor;
            var json = e.NewValue as string;
            if (json != null)
            {
                view.CreateFlowchartModel(json);
            }
        }

        private void CreateFlowchartModel(string json)
        {
            _diagramViewModel.IsLoading = true;

            _diagramViewModel.Items.Clear();
            _diagramViewModel.ToObject(json);           
           
            _diagramViewModel.Init(true);
            _diagramViewModel.IsLoading = false;
        }

        public static readonly DependencyProperty GetDataFuncProperty =
        DependencyProperty.Register(nameof(GetDataFunc),
                                   typeof(Func<string>),
                                   typeof(MindEditor),
                                   new FrameworkPropertyMetadata(null));
        public Func<string> GetDataFunc
        {
            get
            {
                return (Func<string>)this.GetValue(GetDataFuncProperty);
            }
            set
            {
                this.SetValue(GetDataFuncProperty, value);
            }
        }

        public Func<string> GetData
        {
            get
            {
                return new Func<string>(() => _diagramViewModel.ToJson());
            }
        }


        public static readonly DependencyProperty ModeProperty =
         DependencyProperty.Register(nameof(Mode),
                                    typeof(string),
                                    typeof(MindEditor),
                                    new FrameworkPropertyMetadata("Edit", OnModeChanged));

        public string Mode
        {
            get
            {
                return (string)GetValue(ModeProperty);
            }
            set
            {
                SetValue(ModeProperty, value);
            }
        }

        private static void OnModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as MindEditor;
            var mode = e.NewValue as string;
            if (mode != "Edit")
            {
                view._diagramViewModel.IsReadOnly = true;
            }
            else
            {
                view._diagramViewModel.IsReadOnly = false;
            }
        }

        #region ToolBox
        /// <summary>
        /// 附加组件模板
        /// </summary>
        public static readonly DependencyProperty ToolBoxProperty = DependencyProperty.Register(
            nameof(ToolBox), typeof(ControlTemplate), typeof(MindEditor), new FrameworkPropertyMetadata(default(ControlTemplate)));

        public ControlTemplate ToolBox
        {
            get
            {
                return (ControlTemplate)GetValue(ToolBoxProperty);
            }
            set
            {
                SetValue(ToolBoxProperty, value);
            }
        }
        #endregion

        #region SelectedObject

        public static readonly DependencyProperty SelectedObjectProperty = DependencyProperty.Register(nameof(SelectedObject), typeof(object), typeof(MindEditor), new UIPropertyMetadata(default(object)));
        public object SelectedObject
        {
            get
            {
                return (object)GetValue(SelectedObjectProperty);
            }
            set
            {
                SetValue(SelectedObjectProperty, value);
            }
        }

        #endregion 
    }
}
