﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConnectingLineStraight : IPathGenerator
    {
        public PathGeneratorResult Get(IDiagramViewModel _, ConnectionViewModel link, PointBase[] route, PointBase source, PointBase target)
        {
            return PathGenerators.Straight(_, link, route, source, target);
        }
    }
}
