﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class RouterFishBone : IRouter
    {
        public PointBase[] Get(IDiagramViewModel _, ConnectionViewModel link)
        {
            return Routers.FishBone(_, link);
        }
    }
}
