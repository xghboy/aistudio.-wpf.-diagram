﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConnectionPathConverter : IMultiValueConverter
    {
        static ConnectionPathConverter()
        {
            Instance = new ConnectionPathConverter();
        }

        public static ConnectionPathConverter Instance
        {
            get;
            private set;
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            GeometryGroup geometryGroup = new GeometryGroup();
            if (values[0] is PathGeneratorResult result)
            {
                foreach (var path in result.Paths)
                {
                    PathGeometry pathGeometry = PathGeometry.CreateFromGeometry(Geometry.Parse(path));
                    geometryGroup.Children.Add(pathGeometry);
                }
            }

            return geometryGroup;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
