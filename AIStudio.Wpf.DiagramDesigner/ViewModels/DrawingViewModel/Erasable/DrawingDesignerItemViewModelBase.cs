﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using System.Windows.Controls;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DrawingDesignerItemViewModelBase : DesignerItemViewModelBase
    {
        public DrawingDesignerItemViewModelBase() : base()
        {

        }

        public DrawingDesignerItemViewModelBase(IDiagramViewModel root, DrawMode drawMode, Point startPoint, bool erasable) : base(root)
        {
            DrawMode = drawMode;
            Points = new List<Point> { startPoint };
            Erasable = erasable;
            DisableSelected = Erasable;
            if (Erasable)
            {
                if (Root?.DrawModeViewModel != null)
                {
                    this.ColorViewModel = CopyHelper.Mapper(Root.DrawModeViewModel.DrawingColorViewModel);
                }
                else
                {
                    this.ColorViewModel = CopyHelper.Mapper(_service.DrawModeViewModel.DrawingColorViewModel);
                }
            }
        }

        public DrawingDesignerItemViewModelBase(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public DrawingDesignerItemViewModelBase(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new DrawingDesignerItemBase(this);
        }

        protected override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);           
        }

        protected override void InitNew()
        {
            ClearConnectors();
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is DrawingDesignerItemBase designer)
            {
                this.Erasable = designer.Erasable;
                this.Geometry = Geometry.Parse(designer.Geometry).GetFlattenedPathGeometry();
                this.Points = designer.Points;
                this.DrawMode = designer.DrawMode;
                this.DisableSelected = Erasable;
            }
        }

        public virtual bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            return true;
        }

        public virtual bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }

        public virtual bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            if (IsFinish)
            {
                UpdateLocation();

                if (Geometry != null)
                {
                    Geometry.Transform = new TranslateTransform(0 - Left, 0 - Top);

                    if (Erasable)
                    {
                        var aPen = new Pen(ColorViewModel.LineColor.ToBrush(), ColorViewModel.LineWidth);
                        aPen.DashStyle = new DashStyle(StrokeDashArray.Dash[(int)ColorViewModel.LineDashStyle], 1);
                        Geometry = Geometry.GetWidenedPathGeometry(aPen); //可擦除，需要把Geometry转成几何图像，所以不能有填充色
                    }
                }
            }
            return true;
        }

        public virtual bool Erase(Geometry erase)
        {
            if (Erasable && Keyboard.IsKeyDown(Key.LeftShift) == false)
            {
                erase.Transform = new TranslateTransform(0 - Left, 0 - Top);
                Geometry = Geometry.Combine(Geometry, erase, GeometryCombineMode.Exclude, null);

                if (Geometry.IsEmpty())
                    return true;
            }
            else
            {
                if (this.GetBounds().IntersectsWith(erase.Bounds))//相交
                {
                    return true;
                }
            }
            return false;
        }

        public bool Erasable
        {
            get; set;
        }

        private Geometry _geometry;
        public Geometry Geometry
        {
            get
            {
                return _geometry;
            }
            set
            {
                SetProperty(ref _geometry, value);
            }
        }

        public bool IsFinish
        {
            get; set;
        }

        private List<Point> _points;
        public List<Point> Points
        {
            get
            {
                return _points;
            }
            set
            {
                SetProperty(ref _points, value);
            }
        }

        public DrawMode DrawMode
        {
            get; set;
        }

        public void UpdateLocation()
        {
            ItemWidth = Geometry.Bounds.Width + ColorViewModel.LineWidth * 2;
            ItemHeight = Geometry.Bounds.Height + ColorViewModel.LineWidth * 2;
            Left = Geometry.Bounds.Left;
            Top = Geometry.Bounds.Top;
        }

    }
}
