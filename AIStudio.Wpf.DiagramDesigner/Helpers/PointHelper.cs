﻿using System;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class PointHelper
    {
        public static PointBase GetPointForConnector(FullyCreatedConnectorInfo connector, bool middle = false)
        {
            PointBase point = new PointBase();
            if (connector == null || connector.DataItem == null)
            {
                return point;
            }

            if (connector.IsInnerPoint)
            {
                point = new PointBase(connector.DataItem.Left + connector.DataItem.ItemWidth * connector.XRatio - connector.ConnectorWidth / 2,
                        connector.DataItem.Top + connector.DataItem.ItemHeight * connector.YRatio - connector.ConnectorHeight / 2);
            }
            else if (connector.IsPortless)
            {
                point = connector.DataItem.MiddlePosition;
            }
            else
            {
                switch (connector.Orientation)
                {
                    case ConnectorOrientation.Left:
                        point = new PointBase(connector.DataItem.Left - connector.ConnectorWidth / 2, connector.DataItem.Top + (connector.DataItem.ItemHeight / 2) - connector.ConnectorHeight / 2);
                        break;
                    case ConnectorOrientation.TopLeft:
                        point = new PointBase(connector.DataItem.Left - connector.ConnectorWidth / 2, connector.DataItem.Top - connector.ConnectorHeight / 2);
                        break;
                    case ConnectorOrientation.Top:
                        point = new PointBase(connector.DataItem.Left + (connector.DataItem.ItemWidth / 2) - connector.ConnectorWidth / 2, connector.DataItem.Top - connector.ConnectorHeight / 2);
                        break;
                    case ConnectorOrientation.TopRight:
                        point = new PointBase(connector.DataItem.Left + connector.DataItem.ItemWidth - connector.ConnectorWidth / 2, connector.DataItem.Top - connector.ConnectorHeight / 2);
                        break;
                    case ConnectorOrientation.Right:
                        point = new PointBase(connector.DataItem.Left + connector.DataItem.ItemWidth - connector.ConnectorWidth / 2, connector.DataItem.Top + (connector.DataItem.ItemHeight / 2) - connector.ConnectorHeight / 2);
                        break;
                    case ConnectorOrientation.BottomRight:
                        point = new PointBase(connector.DataItem.Left + connector.DataItem.ItemWidth - connector.ConnectorWidth / 2, connector.DataItem.Top + connector.DataItem.ItemHeight - connector.ConnectorHeight / 2);
                        break;
                    case ConnectorOrientation.Bottom:
                        point = new PointBase(connector.DataItem.Left + (connector.DataItem.ItemWidth / 2) - connector.ConnectorWidth / 2, connector.DataItem.Top + connector.DataItem.ItemHeight - connector.ConnectorHeight / 2);
                        break;
                    case ConnectorOrientation.BottomLeft:
                        point = new PointBase(connector.DataItem.Left - connector.ConnectorWidth / 2, connector.DataItem.Top + connector.DataItem.ItemHeight - connector.ConnectorHeight / 2);
                        break;
                    default:
                        point = new PointBase(connector.DataItem.Left + (connector.DataItem.ItemWidth / 2) - connector.ConnectorWidth / 2, connector.DataItem.Top + (connector.DataItem.ItemHeight / 2) - connector.ConnectorHeight / 2);
                        break;
                }
            }

            if (middle)
            {
                point.X = point.X + connector.ConnectorWidth / 2;
                point.Y = point.Y + connector.ConnectorHeight / 2;
            }
            //旋转后的坐标
            var newX = (point.X - connector.DataItem.MiddlePosition.X) * Math.Cos(connector.DataItem.Angle * Math.PI / 180) - (point.Y - connector.DataItem.MiddlePosition.Y) * Math.Sin(connector.DataItem.Angle * Math.PI / 180) + connector.DataItem.MiddlePosition.X;
            var newY = (point.Y - connector.DataItem.MiddlePosition.Y) * Math.Cos(connector.DataItem.Angle * Math.PI / 180) - (point.X - connector.DataItem.MiddlePosition.X) * Math.Sin(connector.DataItem.Angle * Math.PI / 180) + connector.DataItem.MiddlePosition.Y;
            //放大缩小后的坐标

            newX = (newX - connector.DataItem.MiddlePosition.X) * connector.DataItem.ScaleX + connector.DataItem.MiddlePosition.X;
            newY = (newY - connector.DataItem.MiddlePosition.Y) * connector.DataItem.ScaleY + connector.DataItem.MiddlePosition.Y;
            return new PointBase(newX, newY);
        }


    }
}
