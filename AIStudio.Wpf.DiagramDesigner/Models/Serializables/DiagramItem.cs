﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using AIStudio.Wpf.DiagramDesigner.Models;
using Newtonsoft.Json;

namespace AIStudio.Wpf.DiagramDesigner
{
    [Serializable]
    public class DiagramItem
    {
        public DiagramItem()
        {

        }

        public DiagramItem(IDiagramViewModel diagramView)
        {
            Name = diagramView.Name;
            DiagramType = diagramView.DiagramType;
            ShowGrid = diagramView.DiagramOption.LayoutOption.ShowGrid;
            PhysicalGridCellSize = diagramView.DiagramOption.LayoutOption.PhysicalGridCellSize;
            CellHorizontalAlignment = diagramView.DiagramOption.LayoutOption.CellHorizontalAlignment;
            CellVerticalAlignment = diagramView.DiagramOption.LayoutOption.CellVerticalAlignment;
            PageSizeOrientation = diagramView.DiagramOption.LayoutOption.PageSizeOrientation;
            PhysicalPageSize = diagramView.DiagramOption.LayoutOption.PhysicalPageSize;
            PageSizeType = diagramView.DiagramOption.LayoutOption.PageSizeType;
            PhysicalGridMarginSize = diagramView.DiagramOption.LayoutOption.PhysicalGridMarginSize;
            GridColor = diagramView.DiagramOption.LayoutOption.GridColor;
            AllowDrop = diagramView.DiagramOption.LayoutOption.AllowDrop;
        }

        [XmlAttribute]
        public string Name
        {
            get; set;
        }

        [XmlAttribute]
        public DiagramType DiagramType
        {
            get; set;
        }

        [XmlAttribute]
        public bool ShowGrid
        {
            get; set;
        }

        [XmlIgnore]
        public Size PhysicalGridCellSize
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("GridCellSize")]
        public string XmlGridCellSize
        {
            get
            {
                return SerializeHelper.SerializeSize(PhysicalGridCellSize);
            }
            set
            {
                PhysicalGridCellSize = SerializeHelper.DeserializeSize(value);
            }
        }

        [XmlAttribute]
        public CellHorizontalAlignment CellHorizontalAlignment
        {
            get; set;
        }

        [XmlAttribute]
        public CellVerticalAlignment CellVerticalAlignment
        {
            get; set;
        }

        [XmlAttribute]
        public PageSizeOrientation PageSizeOrientation
        {
            get; set;
        }

        [XmlIgnore]
        public Size PhysicalPageSize
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("PageSize")]
        public string XmlPageSize
        {
            get
            {
                return SerializeHelper.SerializeSize(PhysicalPageSize);
            }
            set
            {
                PhysicalPageSize = SerializeHelper.DeserializeSize(value);
            }
        }

        [XmlAttribute]
        public PageSizeType PageSizeType
        {
            get; set;
        }

        [XmlIgnore]
        public Size PhysicalGridMarginSize
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("GridMarginSize")]
        public string XmlGridMarginSize
        {
            get
            {
                return SerializeHelper.SerializeSize(PhysicalGridMarginSize);
            }
            set
            {
                PhysicalGridMarginSize = SerializeHelper.DeserializeSize(value);
            }
        }

        [XmlIgnore]
        public Color GridColor
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("GridColor")]
        public string XmlGridColor
        {
            get
            {
                return SerializeHelper.SerializeColor(GridColor);
            }
            set
            {
                GridColor = SerializeHelper.DeserializeColor(value);
            }
        }

        [XmlAttribute]
        public bool AllowDrop
        {
            get; set;
        }

        [XmlArray]
        public List<SerializableItem> DesignerItems { get; set; } = new List<SerializableItem>();

        [XmlArray]
        public List<SerializableItem> Connections { get; set; } = new List<SerializableItem>();

        [XmlAttribute]
        public string Thumbnail
        {
            get; set;
        }
    }
}
