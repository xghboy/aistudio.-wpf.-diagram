﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DrawingDesignerItemBase : DesignerItemBase
    {
        public DrawingDesignerItemBase()
        {

        }
        public DrawingDesignerItemBase(DrawingDesignerItemViewModelBase item) : base(item)
        {
            this.Erasable = item.Erasable;
            this.Geometry = item.Geometry.ToString();
            this.Points = item.Points;
            this.DrawMode = item.DrawMode;
        }

        [XmlAttribute]
        public bool Erasable
        {
            get; set;
        }

        [XmlAttribute]
        public string Geometry
        {
            get; set;
        }

        [XmlIgnore]
        public List<Point> Points
        {
            get; set;
        }

        [JsonIgnore]
        [XmlElement("Points")]
        public string XmlPoints
        {
            get
            {
                return SerializeHelper.SerializePointList(Points);
            }
            set
            {
                Points = SerializeHelper.DeserializePointList(value);
            }
        }

        [XmlAttribute]
        public DrawMode DrawMode
        {
            get; set;
        }
    }
}
