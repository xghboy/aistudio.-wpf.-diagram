﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AIStudio.Wpf.DiagramDesigner.Controls
{
    public class DiagramTabControl : TabControl
    {
        public DiagramTabControl()
        {
            // Attach event handler to TabControl.SelectionChanged event
            this.SelectionChanged += DiagramTabControl_SelectionChanged;
            this.IsVisibleChanged += DiagramTabControl_IsVisibleChanged;
        }

        private void DiagramTabControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            foreach (var item in this.Items)
            {
                if (item is DiagramViewModel viewModel)
                {
                    viewModel.ShowSearch = false;
                }
            }
        }

        private void DiagramTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Iterate through all TabItems and manually set the Visibility property
            foreach (var item in this.Items)
            {
                if (item is DiagramViewModel viewModel)
                {
                    if (item != this.SelectedItem)
                    {
                        viewModel.ShowSearch = false;
                    }
                }
            }
        }
    }
}