﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class InnerPortViewModel : BaseViewModel
    {
        public InnerPortViewModel()
        {
            Title = "InnerPort";
            Info = "You can add connection points inside a node";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            node1.ClearConnectors();
            var port1 = new FullyCreatedConnectorInfo(DiagramViewModel, node1, ConnectorOrientation.Right, true) { XRatio = 0.5, YRatio = 0.5 };
            node1.AddConnector(port1);

            DiagramViewModel.Add(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 300, Text = "2" };
            node2.ClearConnectors();
            var port2_1 = new FullyCreatedConnectorInfo(DiagramViewModel, node2, ConnectorOrientation.Top, true) { XRatio = 0.2, YRatio = 0.8 };
            var port2_2 = new FullyCreatedConnectorInfo(DiagramViewModel, node2, ConnectorOrientation.Top, true) { XRatio = 0.8, YRatio = 0.2 };
            node2.AddConnector(port2_1);
            node2.AddConnector(port2_2);
            DiagramViewModel.Add(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Text = "3" };
            node3.ClearConnectors();
            var port3 = new FullyCreatedConnectorInfo(DiagramViewModel, node3, ConnectorOrientation.Bottom, true) { XRatio = 0.5, YRatio = 0.5 };
            node3.AddConnector(port3);
            DiagramViewModel.Add(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, port1, port2_1, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, port2_2, port3, DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            DiagramViewModel.Add(connector2);
        }
    }
}
