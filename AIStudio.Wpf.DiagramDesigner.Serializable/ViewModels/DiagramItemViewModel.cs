﻿using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner.Serializable.ViewModels
{
    public class DiagramItemViewModel : DesignerItemViewModelBase
    {
        public DiagramItemViewModel() : this(null)
        {
        }

        public DiagramItemViewModel(IDiagramViewModel root) : base(root)
        {
        }

        public DiagramItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public DiagramItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {
        }


        public virtual DiagramNode ToDiagram()
        {
            return new DiagramNode();
        }
    }
}
